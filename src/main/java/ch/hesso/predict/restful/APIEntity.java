/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.commons.AbstractEntity;
import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.restful.visitors.APIEntityVisitable;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public abstract class APIEntity extends AbstractEntity implements APIEntityVisitable {

	/**
	 *
	 */
	public static final String BASE_API_URL = BASE_HOST_URL + "/core-rest";

	//
	@ApiModelProperty (value = "More pages available", position = 101)
	private boolean hasMore;

	//
	@ApiModelProperty (value = "Whether this modification comes in bulk or not", position = 102)
	private boolean bulk;

	/**
	 * @return
	 */
	@ApiModelProperty (value = "Modification type occurred on Entity", position = 104)
	@Override
	public HTTPMethod getMethod () {
		return super.getMethod ();
	}

	/**
	 * @return
	 */
	@ApiModelProperty (value = "Entity Id", position = 100)
	@Override
	public String getId () {
		return super.getId ();
	}

	@XmlTransient
	private String sessionId;

	/**
	 * @return
	 */
	public boolean isHasMore () {
		return hasMore;
	}

	/**
	 * @param hasMore
	 */
	public void setHasMore ( final boolean hasMore ) {
		this.hasMore = hasMore;
	}

	/**
	 * @return
	 */
	public boolean isBulk () {
		return bulk;
	}

	/**
	 * @param bulk
	 */
	public void setBulk ( final boolean bulk ) {
		this.bulk = bulk;
	}

	public abstract String resourceName ();

	public String getSessionId () {
		return sessionId;
	}

	public void setSessionId ( final String sessionId ) {
		this.sessionId = sessionId;
	}
}
