/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = Recommendation.ROOT_NAME)
public class Recommendation extends APIEntity {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "recommendation";

	public static final String RESOURCE_PATH = "recommendations";

	@ApiModelProperty (value = "Conference acronym", position = 1)
	private Set<String> conferenceAcronym = new TreeSet<> ();

	@ApiModelProperty (value = "Conference keywords", position = 2)
	private Set<String> keywords = new TreeSet<> ();

	@ApiModelProperty (value = "Conference title", position = 3)
	private Set<String> conferenceTitle = new TreeSet<> ();

	@ApiModelProperty (value = "Start year to analyze", position = 4)
	private int startYear;

	@ApiModelProperty (value = "End year to analyze", position = 5)
	private int endYear;

	public Set<String> getConferenceAcronym () {
		return conferenceAcronym;
	}

	public void setConferenceAcronym ( final Set<String> conferenceAcronym ) {
		this.conferenceAcronym = conferenceAcronym;
	}

	public Set<String> getKeywords () {
		return keywords;
	}

	public void setKeywords ( final Set<String> keywords ) {
		this.keywords = keywords;
	}

	public int getStartYear () {
		return startYear;
	}

	public void setStartYear ( final int startYear ) {
		this.startYear = startYear;
	}

	public int getEndYear () {
		return endYear;
	}

	public void setEndYear ( final int endYear ) {
		this.endYear = endYear;
	}

	public Set<String> getConferenceTitle () {
		return conferenceTitle;
	}

	public void setConferenceTitle ( final Set<String> conferenceTitle ) {
		this.conferenceTitle = conferenceTitle;
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

	@Override
	public void accept ( final APIEntityVisitor visitor ) {

	}


	@Override
	public boolean equals ( final Object o ) {
		if ( this == o ) return true;
		if ( o == null || getClass () != o.getClass () ) return false;

		final Recommendation that = (Recommendation) o;

		if ( endYear != that.endYear ) return false;
		if ( startYear != that.startYear ) return false;
		if ( conferenceAcronym != null ? !conferenceAcronym.equals ( that.conferenceAcronym ) : that.conferenceAcronym != null )
			return false;
		if ( conferenceTitle != null ? !conferenceTitle.equals ( that.conferenceTitle ) : that.conferenceTitle != null )
			return false;
		if ( keywords != null ? !keywords.equals ( that.keywords ) : that.keywords != null ) return false;

		return true;
	}

	@Override
	public int hashCode () {
		int result = 7;
		for ( String acronym : conferenceAcronym ) {
			result = 31 * result + acronym.hashCode ();
		}
		for ( String keyword : keywords ) {
			result = 31 * result + keyword.hashCode ();
		}
		for ( String titles : conferenceTitle ) {
			result = 31 * result + titles.hashCode ();
		}
		result = 31 * result + startYear;
		result = 31 * result + endYear;
		return result;
	}
}
