/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = Plugin.ROOT_NAME)
public class Plugin extends APIEntity {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "plugin";

	/**
	 * REST API base path
	 */
	public static final String RESOURCE_PATH = "plugins";

	//
	private String name;

	//
	private String webSocket;

	//
	private String description;

	//
	private Map<String, String> webSockets;

	//
	private Map<String, Set<HTTPMethod>> registrations;

	public String getName () {
		return name;
	}

	public void setName ( final String name ) {
		this.name = name;
	}

	public String getDescription () {
		return description;
	}

	public void setDescription ( final String description ) {
		this.description = description;
	}

	public String getWebSocket () {
		return webSocket;
	}

	public void setWebSocket ( final String webSocket ) {
		this.webSocket = webSocket;
	}

	public Map<String, String> getWebSockets () {
		return webSockets;
	}

	public void setWebSockets ( final Map<String, String> webSockets ) {
		this.webSockets = webSockets;
	}

	public Map<String, Set<HTTPMethod>> getRegistrations () {
		return registrations;
	}

	public void setRegistrations ( final Map<String, Set<HTTPMethod>> registrations ) {
		this.registrations = registrations;
	}

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

	@Override
	public String toString () {
		return "Plugin{" +
				"name='" + name + '\'' +
				", webSocket='" + webSocket + '\'' +
				", webSockets=" + webSockets +
				", registrations=" + registrations +
				'}';
	}
}
