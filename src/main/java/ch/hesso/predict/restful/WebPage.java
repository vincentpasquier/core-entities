/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = WebPage.ROOT_NAME)
public class WebPage extends APIEntity {

	/**
	 *
	 */
	public static final long YEAR = 1000 * 60 * 60 * 24 * 30 * 12;

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "webpage";

	/**
	 * REST API base path
	 */
	public static final String RESOURCE_PATH = "webpages";

	@ApiModelProperty (value = "Webpage URL", position = 1)
	private String url;

	@ApiModelProperty (value = "Webpage content", position = 2)
	private String content;

	@ApiModelProperty (value = "Webpage time", position = 3)
	private long time;

	@ApiModelProperty (value = "Webpage ttl", position = 4)
	private long ttl;

	public WebPage () {
		time = System.currentTimeMillis ();
		ttl = System.currentTimeMillis () + YEAR;
	}

	public String getUrl () {
		return url;
	}

	public void setUrl ( final String url ) {
		this.url = url;
	}

	public String getContent () {
		return content;
	}

	public void setContent ( final String content ) {
		this.content = content;
	}

	public long getTime () {
		return time;
	}

	public void setTime ( final long time ) {
		this.time = time;
	}

	public long getTtl () {
		return ttl;
	}

	public void setTtl ( final long ttl ) {
		this.ttl = ttl;
	}

	/**
	 * @return
	 */
	public boolean valid () {
		return time < ttl;
	}

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

}
