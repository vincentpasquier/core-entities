/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 * SuppressWarnings: Used externally
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = Publication.ROOT_NAME)
@SuppressWarnings ("unused")
public class Publication extends APIEntity {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "publication";

	/**
	 * REST API base path
	 */
	public static final String RESOURCE_PATH = "publications";

	@ApiModelProperty (value = "Associated conference ID", position = 1)
	private String conference;

	@ApiModelProperty (value = "Associated venue ID", position = 2)
	private String venue;

	@ApiModelProperty (value = "Associated DOI", position = 3)
	private String doi;

	@ApiModelProperty (value = "Associated URL", position = 4)
	private String url;

	@ApiModelProperty (value = "Publication title", position = 5)
	private String title;

	@ApiModelProperty (value = "Associated authors IDs", position = 6)
	private Set<String> authors = new HashSet<> ();

	@ApiModelProperty (value = "Publication language", position = 7)
	private String language;

	@ApiModelProperty (value = "How and by who it is published", position = 8)
	private String publisher;

	@ApiModelProperty (value = "Year of publication", position = 10)
	private int year;

	@ApiModelProperty (value = "Keywords associated with the publication", position = 11)
	private Set<String> keywords = new HashSet<> ();

	@ApiModelProperty (value = "Content of the publication (most likely abstract)", position = 12)
	private String content;

	@ApiModelProperty (value = "Editor", position = 13)
	private String editor;

	@ApiModelProperty (value = "Key", position = 13)
	private String key;

	@ApiModelProperty ( value = "Key", position = 13 )
	private String pages;

	@ApiModelProperty ( value = "Book title", position = 14 )
	private String booktitle;

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	public String getVenue () {
		return venue;
	}

	public void setVenue ( final String venue ) {
		this.venue = venue;
	}

	public String getDoi () {
		return doi;
	}

	public void setDoi ( final String doi ) {
		this.doi = doi;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public Set<String> getAuthors () {
		return authors;
	}

	public void setAuthors ( final Set<String> authors ) {
		this.authors = authors;
	}

	public String getLanguage () {
		return language;
	}

	public void setLanguage ( final String language ) {
		this.language = language;
	}

	public String getPublisher () {
		return publisher;
	}

	public void setPublisher ( final String publisher ) {
		this.publisher = publisher;
	}

	public int getYear () {
		return year;
	}

	public void setYear ( final int year ) {
		this.year = year;
	}

	public Set<String> getKeywords () {
		return keywords;
	}

	public void setKeywords ( final Set<String> keywords ) {
		this.keywords = keywords;
	}

	public String getContent () {
		return content;
	}

	public void setContent ( final String content ) {
		this.content = content;
	}

	public String getConference () {
		return conference;
	}

	public void setConference ( final String conference ) {
		this.conference = conference;
	}

	public String getEditor () {
		return editor;
	}

	public void setEditor ( final String editor ) {
		this.editor = editor;
	}

	public String getUrl () {
		return url;
	}

	public void setUrl ( final String url ) {
		this.url = url;
	}

	public String getKey () {
		return key;
	}

	public void setKey ( final String key ) {
		this.key = key;
	}

	public String getPages () {
		return pages;
	}

	public void setPages ( final String pages ) {
		this.pages = pages;
	}

	public String getBooktitle () {
		return booktitle;
	}

	public void setBooktitle ( final String booktitle ) {
		this.booktitle = booktitle;
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

	@Override
	public String toString () {
		return "Publication{" +
				"\nconference='" + conference + '\'' +
				"\n, venue='" + venue + '\'' +
				"\n, doi='" + doi + '\'' +
				"\n, url='" + url + '\'' +
				"\n, title='" + title + '\'' +
				"\n, authors=" + authors +
				"\n, language='" + language + '\'' +
				"\n, publisher='" + publisher + '\'' +
				"\n, year=" + year +
				"\n, keywords=" + keywords +
				"\n, content='" + content + '\'' +
				"\n, editor='" + editor + '\'' +
				"\n, key='" + key + '\'' +
				"\n, pages='" + pages + '\'' +
				"\n}";
	}
}
