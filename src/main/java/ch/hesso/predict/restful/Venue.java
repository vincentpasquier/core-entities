/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = Venue.ROOT_NAME)
public class Venue extends APIEntity {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "venue";

	/**
	 * REST API base path
	 */
	public static final String RESOURCE_PATH = "venues";

	@ApiModelProperty (value = "Conference ID", position = 1)
	private String conference;

	@ApiModelProperty (value = "City and or Country", position = 2)
	private Set<String> location;

	@ApiModelProperty (value = "Conference starting date", position = 3)
	private Date start;

	@ApiModelProperty (value = "Conference ending date", position = 4)
	private Date end;

	@ApiModelProperty (value = "Conference year", position = 5)
	private int year;

	@ApiModelProperty (value = "Publications made at the conference", position = 6)
	private List<String> publications = new ArrayList<> ();

	@ApiModelProperty (value = "Venue key", position = 7)
	private String key;

	@ApiModelProperty (value = "Venue title", position = 8)
	private String title;

	public String getConference () {
		return conference;
	}

	public void setConference ( final String conference ) {
		this.conference = conference;
	}

	public Set<String> getLocation () {
		return location;
	}

	public void setLocation ( final Set<String> location ) {
		this.location = location;
	}

	public Date getStart () {
		return start;
	}

	public void setStart ( final Date start ) {
		this.start = start;
	}

	public Date getEnd () {
		return end;
	}

	public void setEnd ( final Date end ) {
		this.end = end;
	}

	public int getYear () {
		return year;
	}

	public void setYear ( final int year ) {
		this.year = year;
	}

	public List<String> getPublications () {
		return publications;
	}

	public void setPublications ( final List<String> publications ) {
		this.publications = publications;
	}

	public String getKey () {
		return key;
	}

	public void setKey ( final String key ) {
		this.key = key;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	@Override
	public String toString () {
		return "Venue{" +
				"conference='" + conference + '\'' +
				", location=" + location +
				", start=" + start +
				", end=" + end +
				", year=" + year +
				", publications=" + publications +
				", key='" + key + '\'' +
				", title='" + title + '\'' +
				'}';
	}

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}
}
