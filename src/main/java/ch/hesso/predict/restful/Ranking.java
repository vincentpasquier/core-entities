/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;


import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * SuppressWarning: Used externally
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement ( name = Ranking.ROOT_NAME )
@SuppressWarnings ( "unused" )
public class Ranking extends APIEntity {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "ranking";

	@ApiModelProperty ( value = "Ranking title", position = 1 )
	private String title;

	@ApiModelProperty ( value = "Ranking acronym", position = 2 )
	private String acronym;

	@ApiModelProperty ( value = "Ranking category", position = 3 )
	private String category;

	@ApiModelProperty ( value = "Number of publication displayed", position = 4 )
	private int publication;

	@ApiModelProperty ( value = "Conference rating (modified h-index)", position = 5 )
	private int fieldRating;


	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getAcronym () {
		return acronym;
	}

	public void setAcronym ( final String acronym ) {
		this.acronym = acronym;
	}

	public String getCategory () {
		return category;
	}

	public void setCategory ( final String category ) {
		this.category = category;
	}

	public int getPublication () {
		return publication;
	}

	public void setPublication ( final int publication ) {
		this.publication = publication;
	}

	public int getFieldRating () {
		return fieldRating;
	}

	public void setFieldRating ( final int fieldRating ) {
		this.fieldRating = fieldRating;
	}

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

	@Override
	public String toString () {
		return "Ranking{" +
				"title='" + title + '\'' +
				", acronym='" + acronym + '\'' +
				", category='" + category + '\'' +
				", publication=" + publication +
				", fieldRating=" + fieldRating +
				'}';
	}
}
