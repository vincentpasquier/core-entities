/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

/**
 * SuppressWarnings: Used externally
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = Cfp.ROOT_NAME)
@SuppressWarnings (value = "unused")
public class Cfp extends APIEntity {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "cfp";

	/**
	 * REST API base path
	 */
	public static final String RESOURCE_PATH = "calls";

	@ApiModelProperty ( value = "Conference ID", position = 1 )
	private String conferenceId;

	@ApiModelProperty (value = "Keywords", position = 2)
	private Set<String> keywords = new HashSet<> ();

	@ApiModelProperty (value = "Call for paper body", position = 3)
	private String content;

	@ApiModelProperty (value = "Referrers URLs", position = 4)
	private Set<String> referrers = new HashSet<> ();

	@ApiModelProperty (value = "Related CFP IDs", position = 5)
	private Set<String> related = new HashSet<> ();

	@ApiModelProperty (value = "CFP deadlines", position = 6)
	private Map<Deadline, String> deadlines = new HashMap<> ();

	@ApiModelProperty (value = "CFP origin", position = 7)
	private String origin;

	@ApiModelProperty (value = "Conference Website", position = 8)
	private String website;

	@ApiModelProperty (value = "CFP title", position = 8)
	private String title;

	@ApiModelProperty (value = "CFP acronym", position = 8)
	private String acronym;

	@ApiModelProperty ( value = "CFP start", position = 8 )
	private String start;

	@ApiModelProperty ( value = "CFP end", position = 8 )
	private String end;

	@ApiModelProperty ( value = "CFP location", position = 8 )
	private String location;

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	public Map<Deadline, String> getDeadlines () {
		return deadlines;
	}

	public void setDeadlines ( final Map<Deadline, String> deadlines ) {
		this.deadlines = deadlines;
	}

	public Set<String> getKeywords () {
		return keywords;
	}

	public void setKeywords ( final Set<String> keywords ) {
		this.keywords = keywords;
	}

	public String getContent () {
		return content;
	}

	public void setContent ( final String content ) {
		this.content = content;
	}

	public Set<String> getRelated () {
		return related;
	}

	public void setRelated ( final Set<String> related ) {
		this.related = related;
	}

	public Set<String> getReferrers () {
		return referrers;
	}

	public void setReferrers ( final Set<String> referrers ) {
		this.referrers = referrers;
	}

	public String getConferenceId () {
		return conferenceId;
	}

	public void setConferenceId ( final String conferenceId ) {
		this.conferenceId = conferenceId;
	}

	public String getOrigin () {
		return origin;
	}

	public void setOrigin ( final String origin ) {
		this.origin = origin;
	}

	public String getWebsite () {
		return website;
	}

	public void setWebsite ( final String website ) {
		this.website = website;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getAcronym () {
		return acronym;
	}

	public void setAcronym ( final String acronym ) {
		this.acronym = acronym;
	}

	public String getStart () {
		return start;
	}

	public void setStart ( final String start ) {
		this.start = start;
	}

	public String getEnd () {
		return end;
	}

	public void setEnd ( final String end ) {
		this.end = end;
	}

	@Override
	public String toString () {
		return "Cfp{" +
				"\nconferenceId='" + conferenceId + '\'' +
				"\n, keywords=" + keywords +
				"\n, content='" + content + '\'' +
				"\n, referrers=" + referrers +
				"\n, related=" + related +
				"\n, deadlines=" + deadlines +
				"\n, origin='" + origin + '\'' +
				"\n, website='" + website + '\'' +
				"\n, title='" + title + '\'' +
				"\n, acronym='" + acronym + '\'' +
				"\n, start='" + start + '\'' +
				"\n, end='" + end + '\'' +
				"\n, location='" + location + '\'' +
				'}';
	}

	public String getLocation () {
		return location;
	}

	public void setLocation ( final String location ) {
		this.location = location;
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

	/**
	 * CFP possible deadline
	 */
	public enum Deadline {
		REGISTRATION,
		SUBMISSION,
		FINAL_VERSION,
		NOTIFICATION
	}

}
