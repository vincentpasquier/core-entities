/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 * SuppressWarnings: Used externally
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = Person.ROOT_NAME)
@SuppressWarnings (value = "unused")
public class Person extends APIEntity implements Comparable<Person> {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "person";

	/**
	 * REST API base path
	 */
	public static final String RESOURCE_PATH = "people";

	@ApiModelProperty (value = "First name, last name and everything name", position = 1)
	private String name;

	@ApiModelProperty (value = "Person title (Mr, Mrs, ...)", position = 2)
	private String title;

	@ApiModelProperty (value = "Person rank (Dr, Professor, ...)", position = 3)
	private String rank;

	@ApiModelProperty ( value = "Person rank cumulative", position = 4 )
	private int ranking;

	@ApiModelProperty (value = "Country of origin", position = 5)
	private String country;

	@ApiModelProperty (value = "Establishments associated with person (i.e. University)", position = 6)
	private Set<String> establishments = new HashSet<> ();

	@ApiModelProperty ( value = "Associated conference IDs", position = 7 )
	private Set<String> conferences = new HashSet<> ();

	@ApiModelProperty ( value = "Associated publication IDs", position = 8 )
	private Set<String> publications = new HashSet<> ();

	/**
	 *
	 */
	public Person () {

	}

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	public String getName () {
		return name;
	}

	public void setName ( final String name ) {
		this.name = name;
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getRank () {
		return rank;
	}

	public void setRank ( final String rank ) {
		this.rank = rank;
	}

	public int getRanking () {
		return ranking;
	}

	public void setRanking ( final int ranking ) {
		this.ranking = ranking;
	}

	public String getCountry () {
		return country;
	}

	public void setCountry ( final String country ) {
		this.country = country;
	}

	public Set<String> getConferences () {
		return conferences;
	}

	public void setConferences ( final Set<String> conferences ) {
		this.conferences = conferences;
	}

	public Set<String> getEstablishments () {
		return establishments;
	}

	public void setEstablishments ( final Set<String> establishments ) {
		this.establishments = establishments;
	}

	public Set<String> getPublications () {
		return publications;
	}

	public void setPublications ( final Set<String> publications ) {
		this.publications = publications;
	}

	@Override
	public String toString () {
		return "Person{" +
				"name='" + name + '\'' +
				", title='" + title + '\'' +
				", country='" + country + '\'' +
				", rank='" + rank + '\'' +
				'}';
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

	@Override
	public int compareTo ( final Person o ) {
		return Integer.compare ( o.getRanking (), ranking );
	}
}
