/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.restful;

import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import com.wordnik.swagger.annotations.ApiModelProperty;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * SuppressWarnings: Used externally
 *
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
@XmlRootElement (name = Conference.ROOT_NAME)
@SuppressWarnings (value = "unused")
public class Conference extends APIEntity {

	/**
	 * ROOT Name used when encoding/decoding entity
	 */
	public static final String ROOT_NAME = "conference";

	/**
	 * REST API base path
	 */
	public static final String RESOURCE_PATH = "conferences";

	@ApiModelProperty (value = "Conference title", position = 1)
	private String title;

	@ApiModelProperty (value = "Conference URL", position = 2)
	private String url;

	@ApiModelProperty (value = "Summary", position = 3)
	private String acronym;

	@ApiModelProperty (value = "Venues", position = 4)
	private List<String> venues = new ArrayList<> ();

	@ApiModelProperty (value = "CFPs ID", position = 5)
	private List<String> cfp = new ArrayList<> ();

	@ApiModelProperty (value = "Program committee members", position = 6)
	private Set<String> people = new HashSet<> ();

	@ApiModelProperty (value = "Conference category", position = 8)
	private Set<String> category = new HashSet<> ();

	@ApiModelProperty (value = "Conference h-index", position = 9)
	private int hIndex;

	@ApiModelProperty (value = "Conference publication count", position = 10)
	private int publicationCount;

	@ApiModelProperty (value = "Conference keywords", position = 11)
	private Set<String> keywords = new HashSet<> ();

	@ApiModelProperty (value = "Conference key", position = 12)
	private String key;

	@Override
	public void accept ( final APIEntityVisitor visitor ) {
		visitor.visit ( this );
	}

	@Override
	public String toString () {
		return "Conference{" +
				"title='" + title + '\'' +
				", url='" + url + '\'' +
				", acronym='" + acronym + '\'' +
				", venues=" + venues +
				", cfp=" + cfp +
				", people=" + people +
				", category=" + category +
				", hIndex=" + hIndex +
				", publicationCount=" + publicationCount +
				", keywords=" + keywords +
				", key='" + key + '\'' +
				'}';
	}

	public String getTitle () {
		return title;
	}

	public void setTitle ( final String title ) {
		this.title = title;
	}

	public String getUrl () {
		return url;
	}

	public void setUrl ( final String url ) {
		this.url = url;
	}

	public String getAcronym () {
		return acronym;
	}

	public void setAcronym ( final String acronym ) {
		this.acronym = acronym;
	}

	public List<String> getVenues () {
		return venues;
	}

	public void setVenues ( final List<String> venues ) {
		this.venues = venues;
	}

	public Set<String> getPeople () {
		return people;
	}

	public void setPeople ( final Set<String> people ) {
		this.people = people;
	}

	public List<String> getCfp () {
		return cfp;
	}

	public void setCfp ( final List<String> cfp ) {
		this.cfp = cfp;
	}

	public Set<String> getCategory () {
		return category;
	}

	public void setCategory ( final Set<String> category ) {
		this.category = category;
	}

	public int gethIndex () {
		return hIndex;
	}

	public void sethIndex ( final int hIndex ) {
		this.hIndex = hIndex;
	}

	public int getPublicationCount () {
		return publicationCount;
	}

	public void setPublicationCount ( final int publicationCount ) {
		this.publicationCount = publicationCount;
	}

	public Set<String> getKeywords () {
		return keywords;
	}

	public void setKeywords ( final Set<String> keywords ) {
		this.keywords = keywords;
	}

	public String getKey () {
		return key;
	}

	public void setKey ( final String key ) {
		this.key = key;
	}

	@Override
	public String resourceName () {
		return ROOT_NAME;
	}

	public enum ConferenceType {
		SCHOOL,
		WORKSHOP,
		CONGRESS,
		SYMPOSIUM,
		MEETING,
		CONFERENCE
	}

}
